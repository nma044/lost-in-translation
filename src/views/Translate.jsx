import TranslateForm from "../components/Translate/TranslateForm"
import withAuth from "../hoc/withAuth"
import { useUser } from "../Context/UserContext"
import { transalateAdd } from "../Api/translate"
import { storageSave } from "../Utils/Storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"

const Translate = () => {

    const {user, setUser} = useUser()

    const handlerTranslateClicked = async (translate) => {

        const [error, updatedUser] = await transalateAdd(user, translate)
        if ( error != null){
            return
        }

        // Keep UI and server state in sync
        storageSave(STORAGE_KEY_USER, updatedUser)

        // update context state
        setUser(updatedUser)

        console.log(error)
        console.log(updatedUser)

    }

    return(
        <div className="text-center">
            <h1>Translate</h1>
            <section id="translate-options">
                <TranslateForm onTranslate = {handlerTranslateClicked} />
            </section>
        </div>
    )
}
export default withAuth(Translate)