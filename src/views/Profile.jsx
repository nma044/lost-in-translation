import { useEffect } from "react"
import { userById } from "../Api/user"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslateHistory from "../components/Profile/ProfileTranslateHistory"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../Context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../Utils/Storage"

const Profile = () => {

    const {user, setUser} = useUser()

    useEffect (() => {
        const findUser = async () => {
            const [error, latestUser] = await userById(user.id)
            if (error === null){
                storageSave(STORAGE_KEY_USER, latestUser)
                setUser(latestUser)
            }
        }
        //findUser()
    }, [setUser, user.id])
    
    return(
        <div className="text-center">
                <h1>Profile</h1>
                <ProfileHeader username={user.username} />
                <ProfileActions/>
                <ProfileTranslateHistory translations={user.translations}/>
        </div>
    )
}
export default withAuth(Profile)