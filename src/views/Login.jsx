import LoginForm from "../components/Login/LoginForm"

const Login = () => {
    return(
        <div className="text-center">
        <h1>Login</h1>
        <LoginForm />
        </div>
    )
}
export default Login