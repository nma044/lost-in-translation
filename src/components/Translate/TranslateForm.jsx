import { useForm } from "react-hook-form"
import { useState } from "react"

const TranslateForm = ({onTranslate}) => {
    
    const  {register, handleSubmit} = useForm()
    const [Translation,setTranslation] = useState([]);
    
    
    const onSubmit = ({translateNotes}) => 
    {onTranslate(translateNotes)
    const lowerCaseLetters = translateNotes.toLowerCase().split("");
    console.log(lowerCaseLetters)
    setTranslation(lowerCaseLetters);
    }

    return (
        <>
        <form onSubmit = {handleSubmit(onSubmit)}>
        <fieldset>
            <label htmlFor = "translate-notes"> Translate notes: </label>
            <input type = "text" {...register('translateNotes')} placeholder="insert text here"/>
            </fieldset>
 
            <button type="submit">Translate</button>
            <p> { Translation.map((letter) => <img src= {`/individial_signs/${letter}.png` } alt={letter}/>) } </p>
        </form>
        </>
    )
}
export default TranslateForm