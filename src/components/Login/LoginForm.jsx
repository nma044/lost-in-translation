  import { useForm } from 'react-hook-form'
import { loginUser } from  '../../Api/user'
import { useState, useEffect } from 'react'
import { storageSave } from '../../Utils/Storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../Context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import { Button } from 'react-bootstrap';

const usernameConfig={
    required: true,
    minLength: 2
}

const LoginForm = () => {
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm();
    const { user, setUser} = useUser()
    const navigate = useNavigate()

    const [ loading, setLoading ] = useState(false);
    const [apiError, setApiError] = useState(null);

    useEffect(() => {
        if (user!==null){
            navigate('profile')
        }
    }, [user,navigate])

    const onSubmit = async ({username}) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username)
        if(error !== null) {
            setApiError(error)
        }
        if(userResponse!== null){
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false);
    }

    const errorMessage = (() => {
        if (!errors.username) return null;

        if (errors.username.type === 'required'){
            return <span>Username is required</span>
        }
        if (errors.username.type === 'minLength'){
            return <span>Username is too short</span>
        }
    })()

    return (
        <>
        <h2>What is your name?</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset>
                <label htmlFor="username">Username: </label>
                <input type="text" { ...register("username", usernameConfig) } />
                { errorMessage }
            </fieldset>
            <Button type="submit" disabled={loading}>Continue</Button>

            {loading && <p>Logging in...</p>}
            {apiError && <p>{apiError}</p>}
        </form>
        </>
    )
}

export default LoginForm