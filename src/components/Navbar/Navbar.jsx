import { NavLink } from "react-router-dom"
import { useUser } from "../../Context/UserContext"
import Navbar from 'react-bootstrap/Navbar';
import React from "react";
const MyNavbar = () => {
    
    const {user} = useUser()
    
    return (
        <Navbar bg="danger" className="navbar navbar-expand-lg">
            <ul>
                <h3>Lost in translation</h3>
            </ul>

            {user !== null && 

            <div className="navText">
            <p className="translate">
            <NavLink to ="/translate">Translate</NavLink>
                    
            <NavLink className="profile" to="/profile">Profile</NavLink>
            </p>
            </div>} 
        </Navbar>
    )
}
export default MyNavbar