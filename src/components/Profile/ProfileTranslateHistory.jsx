import ProfileTranslateHistoryItem from "./ProfileTranslateHistoryItem"

const ProfileTranslateHistory = ({translations}) => {
    
    const translateList = translations.map(
        (translation,index) => <ProfileTranslateHistoryItem key={ index+'-'+translation } translation={translation} />)

    return(
        <section>
            <h4>Your translations</h4>
            <ul>
                {translateList}
            </ul>
        </section>
    )
}
export default ProfileTranslateHistory