import { Link } from "react-router-dom"
import { storageDelete, storageSave} from "../../Utils/Storage"
import { useUser } from "../../Context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { translateClearHistory } from "../../Api/translate"

const ProfileActions = ({logout}) => {

    const {user, setUser} = useUser()

    const handleLogoutClick = () => {
        if (window.confirm('Are you sure?')) {
            // send av event to the parent!
            storageDelete(STORAGE_KEY_USER) 
            setUser(null)
        }
    }

    const handleClearHistoryClick = async () => {
        if (!window.confirm('Are you sure?')) {
            return
        }
        const [clearError] = await translateClearHistory(user.id) 
        
        if (clearError !== null){
            //Do something about this,
            return
        }
 
        const updatedUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }


    return(
        <ul>
            <Link to="/translate">Translate</Link>
            <br></br>
            <button onClick={handleClearHistoryClick}>Clear history</button>
            <br></br>
            <button onClick={ handleLogoutClick}>Logout</button>
        </ul>

    )
}
export default ProfileActions