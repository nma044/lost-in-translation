import {createHeaders} from "."

const apiUrL = process.env.REACT_APP_API_URL

export const transalateAdd= async (user, translate) => {
    try {
        const response = await fetch(`${apiUrL}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translate]
            })
        })

        if (!response.ok) {
            throw new Error("could not update the translates")
        }
        const result = await response.json()
        return [null, result]
    }
    catch (error) {
        return [error.message, null]

    }
}

export const translateClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiUrL}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if (!response.ok){
            throw new Error('Could not update order')
        }
        const result = await response.json()
        return [null, result]
    } catch (error) {
        return [error.message, null]
    }
 
}